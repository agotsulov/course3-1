package ru.omsu.imit.course3.core;

import org.junit.jupiter.api.Test;
import ru.omsu.imit.course3.animal.UniversalSpine;
import ru.omsu.imit.course3.entities.Animal;
import ru.omsu.imit.course3.entities.Plant;
import ru.omsu.imit.course3.plant.UniversalChlorophyll;

import static org.junit.jupiter.api.Assertions.*;

class EntityTest {

    /*
        Разбить в отделный тест-классы по типу
     */

    @Test
    void testEqualsAnimal(){
        Entity expect = new Animal(new UniversalSpine(1));
        Entity other1 = new Animal(new UniversalSpine(1));
        Entity other2 = new Animal(new UniversalSpine(2));
        assertEquals(expect, expect);
        assertEquals(expect, other1);
        assertNotEquals(expect, other2);
    }


    @Test
    void testEqualsPlant(){
        Entity expect = new Plant(new UniversalChlorophyll(1));
        Entity other1 = new Plant(new UniversalChlorophyll(1));
        Entity other2 = new Plant(new UniversalChlorophyll(2));
        assertEquals(expect, expect);
        assertEquals(expect, other1);
        assertNotEquals(expect, other2);
    }

    @Test
    void testType(){
        Entity expectAnimal = new Animal(new UniversalSpine(1));
        Entity expectPlant = new Plant(new UniversalChlorophyll(1));
        assertEquals(Type.ANIMAL, expectAnimal.getType());
        assertEquals(Type.PLANT, expectPlant.getType());
    }


}