package ru.omsu.imit.course3.nio;

import org.junit.jupiter.api.Test;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;

public class PathsTest {

    @Test
    void testGetUrl() throws URISyntaxException {
        Path path = Paths.get(new URI("file:///D:/Workspace/course3-1/./test.txt"));
        System.out.println(path.toUri().toString());
        System.out.println(path.toString());
    }

    @Test
    void testGetStrings() throws URISyntaxException {
        Path path = Paths.get(".", "test/", "java/");
        System.out.println(path.toUri().toString());
        System.out.println(path.toString());
    }

}