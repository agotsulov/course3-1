package ru.omsu.imit.course3.nio;


import org.junit.jupiter.api.Test;
import ru.omsu.imit.course3.trianee.Trainee;
import ru.omsu.imit.course3.trianee.TrainingException;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.Scanner;


import static org.junit.jupiter.api.Assertions.*;

public class TestNio {

    @Test
    void first() throws IOException, TrainingException {
        Trainee actual = new Trainee("aasgjmkfdsnlgklj2321", "asfhasouibghn315213", 5);
        Trainee.writeTraineeToTextFileOneLine(new File("test/test1.txt"), actual);

        assertEquals(actual, Nio.writeTraineeToFile("test/test1.txt"));
    }


    @Test
    void second() throws TrainingException, IOException {
        Trainee actual = new Trainee("41314abnsbklj2321", "ggbnfsabasouibghn315213", 5);
        Trainee.writeTraineeToTextFileOneLine(new File("test/test.txt"), actual);

        assertEquals(actual, Nio.readTraineeToFileMappedByteBuffer("test/test.txt"));
    }

    @Test
    void three() throws IOException {
        Path filePath = FileSystems.getDefault().getPath(".","test/", "test.txt");

        Nio.writeHundredInFile(filePath);

        Scanner sc = new Scanner(new File("./test/test.txt"));
        while (sc.hasNext()){
            System.out.println(sc.next());
        }

    }

    @Test
    void four() throws IOException, TrainingException {
        Trainee expect = new Trainee("Ivan", "Ivanov", 2);
        Nio.serializeTrainee(expect);
    }

    @Test
    void six() throws IOException {
        Path rootPath = Paths.get("test");

        Nio.renameDatToBin(rootPath);
    }

}
