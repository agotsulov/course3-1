package ru.omsu.imit.course3.nio;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import static org.junit.jupiter.api.Assertions.*;

public class FilesTest {


    @Test
    void testExists() {
        Path path = Paths.get("src/test/java/");
        assertTrue(Files.exists(path));
        path = Paths.get("ikjnblgfasjbklgjlkbsa/asgs");
        assertFalse(Files.exists(path));
    }


    @Test
    void testCreateDir() throws IOException {
        Path path = Paths.get("test/subdir");
        assertFalse(Files.exists(path));
        Files.createDirectory(path);
        assertTrue(Files.exists(path));
    }

    @Test
    void testCreateFile() throws IOException {
        Path path = Paths.get("test/lol.txt");
        assertFalse(Files.exists(path));
        Files.createFile(path);
        assertTrue(Files.exists(path));
    }

    @Test
    void testDelete() throws IOException {
        Path path = Paths.get("test/subdir");
        assertTrue(Files.exists(path));
        Files.delete(path);
        assertFalse(Files.exists(path));
        path = Paths.get("test/lol.txt");
        Files.deleteIfExists(path);
        assertFalse(Files.exists(path));
    }

    @Test
    void testMove() throws IOException {
        Path sPath = Paths.get("test/t.txt");
        Path dPath = Paths.get("test/t-move.txt");

        if (Files.notExists(sPath)) {
            Files.createFile(sPath);
        }
        Files.move(sPath, dPath, StandardCopyOption.REPLACE_EXISTING);
    }

    @Test
    void testCopy() throws IOException {
        Path sPath = Paths.get("test/t.txt");
        Path dPath = Paths.get("test/t-copy.txt");

        if (Files.notExists(sPath)) {
            Files.createFile(sPath);
        }
        Files.move(sPath, dPath, StandardCopyOption.REPLACE_EXISTING);
    }


}
