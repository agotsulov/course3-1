package ru.omsu.imit.course3.nio;

import org.junit.jupiter.api.Test;

import java.nio.file.FileSystems;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.*;

public class PathTest {

    @Test
    void test() {
        Path filePath = FileSystems.getDefault().getPath(".", "/test/", "test.txt");
        System.out.println(filePath.getFileName().toString());
        System.out.println(filePath.toFile().toString());
        System.out.println(filePath.toUri().toString());
        System.out.println(filePath.toAbsolutePath().toString());
        System.out.println(filePath.isAbsolute());
        for (Path element : filePath) {
            System.out.println(element);
        }
    }

    @Test
    void testNormalize() {
        Path path = FileSystems.getDefault().getPath(".","/test/",  "test.txt");
        assertEquals("test\\test.txt", path.normalize().toString());
    }

    @Test
    void testStartWith() {
        Path path = FileSystems.getDefault().getPath(".","/test/",  "test.txt");
        assertTrue(path.startsWith("."));
        path = FileSystems.getDefault().getPath("test.txt");
        assertFalse(path.startsWith(FileSystems.getDefault().getPath(".")));
    }

    @Test
    void testEndWith() {
        Path path = FileSystems.getDefault().getPath(".","/test/",  "test.txt");
        assertTrue(path.endsWith("test.txt"));
    }


    @Test
    void testResolve() {
        Path path = FileSystems.getDefault().getPath(".", "/test/");
        assertEquals(".\\test\\test.txt", path.resolve("test.txt").toString());
    }
}
