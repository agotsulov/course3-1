package ru.omsu.imit.course3.java8;

import org.junit.jupiter.api.Test;
import ru.omsu.imit.course3.person.Person;
import ru.omsu.imit.course3.person.PersonWithAge;

import java.util.*;
import java.util.function.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class FunctionsTest {

    private MyFunction<String, List<String>> split = (s) -> Arrays.asList(s.split(" "));

    private MyFunction<List<?>, Integer> count = (list) -> list.size();


    private MyFunction<List<?>, Integer> countMR = List::size;

    @Test
    void testSplitAndCount() {
        String string = "aaa bbbb 123";

        List<String> expect = new ArrayList<>();
        expect.add("aaa");
        expect.add("bbbb");
        expect.add("123");

        List<String> actual = split.apply(string);

        assertEquals(expect, actual);
        assertEquals(Integer.valueOf(3), count.apply(actual));
    }

    private MyFunction<String, Integer> splitAndCountA = (s) -> split.andThen(count).apply(s);

    private MyFunction<String, Integer> splitAndCountB = (s) -> count.compose(split).apply(s);

    private MyFunction<String, Person> create = Person::new;

    private IntBinaryOperator max = Math::max;

    private Supplier<Date> getCurrentDate = Date::new;

    private IntPredicate isEven = (n) -> n % 2 == 0;

    private BiPredicate<Integer, Integer> areEqual = Integer::equals;

    @FunctionalInterface
    interface MyFunction<T,K> extends Function<T,K> {
        K apply(T arg);
    }

    public void transform(IntStream stream, IntUnaryOperator op) {
        stream.map(op).forEach(System.out::println);
    }

    public void transformParallel(IntStream stream, IntUnaryOperator op) {
        stream.parallel().map(op).forEach(System.out::println);
    }

    @Test
    void testTransform() {
        IntStream intStream = IntStream.of( 1 , 2, 4, 6 , 123, 12, 6, 0, 9);
        transform(intStream, (a) -> a * 2);
    }

    @Test
    void testTransformParallel() {
        IntStream intStream = IntStream.of( 1 , 2, 4, 6 , 123, 12, 6, 0, 9);
        transformParallel(intStream, (a) -> a * 2);
    }

    @Test
    void testPersonStream () {
        List<PersonWithAge> persons = new ArrayList<>();

        persons.add(new PersonWithAge("ffffff",-10));
        persons.add(new PersonWithAge("Aaaaaaagsgasdfg",40));
        persons.add(new PersonWithAge("Aa11afsdgadfgsadg1a",60));
        persons.add(new PersonWithAge("Aababdsa",60));
        persons.add(new PersonWithAge("Aababdsa",616));
        persons.add(new PersonWithAge("Aababdsa",6130));
        persons.add(new PersonWithAge("Aababdsa",61630));
        persons.add(new PersonWithAge("Aaba",-610));
        persons.add(new PersonWithAge("Aaa",124));
        persons.add(new PersonWithAge("Aaaa",6135));
        persons.add(new PersonWithAge("Aaasfhadsfhadfha",-10));
        persons.add(new PersonWithAge("Aahfaa",121));

        persons.stream().filter((p) -> p.getAge() > 30).collect(Collectors.toSet()).stream().sorted(Comparator.comparingInt(a -> a.getName().length())).forEach(System.out::println);

    }

    @Test
    void testPersonStreamWithEquals () {
        List<PersonWithAge> persons = new ArrayList<>();

        persons.add(new PersonWithAge("ffffff",-10));
        persons.add(new PersonWithAge("Aaaaaaagsgasdfg",40));
        persons.add(new PersonWithAge("Aa11afsdgadfgsadg1a",60));
        persons.add(new PersonWithAge("Aababdsa",60));
        persons.add(new PersonWithAge("Aababdsa",616));
        persons.add(new PersonWithAge("Aababdsa",6130));
        persons.add(new PersonWithAge("Aababdsa",61630));
        persons.add(new PersonWithAge("Aaba",-610));
        persons.add(new PersonWithAge("Aaa",124));
        persons.add(new PersonWithAge("Aaaa",6135));
        persons.add(new PersonWithAge("Aaasfhadsfhadfha",-10));
        persons.add(new PersonWithAge("Aahfaa",121));

        persons.stream().filter((p) -> p.getAge() > 30).collect(Collectors.groupingBy(Person::getName)).entrySet().stream().sorted(Comparator.comparingInt(a -> a.getKey().length())).forEach(System.out::println);

    }

    public int sum(List<Integer> list) {
        return list.stream().reduce((s1, s2) -> s1 + s2).get();
    }

    @Test
    void testSum() {
        List<Integer> i = new ArrayList<>();
        i.add(10);
        i.add(21);
        i.add(6);
        i.add(-3);

        assertEquals(34,sum(i));
    }


    public int product(List<Integer> list) {
        return list.stream().reduce((s1, s2) -> s1 * s2).get();
    }

    @Test
    void testProduct() {
        List<Integer> i = new ArrayList<>();
        i.add(2);
        i.add(3);
        i.add(4);

        assertEquals(24,product(i));
    }

}


