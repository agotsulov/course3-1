package ru.omsu.imit.course3;

import org.junit.jupiter.api.Test;
import ru.omsu.imit.course3.core.Color;

import java.util.*;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

public class ListsSpeedTest {

    @Test
    void testSpeedCollectionsGet(){
        List<Integer> arrList = new ArrayList<>();
        List<Integer> linkList = new LinkedList<>();


        Random random = new Random();

        int count = 10000;

        for(int i = 0;i < count;i++){
            int r = random.nextInt();
            arrList.add(r);
            linkList.add(r);
        }

        long startTimeArr = System.nanoTime();
        for(int i = 0;i < count;i++)
            arrList.get(i);
        long estimatedTimeArr = System.nanoTime() - startTimeArr;

        long startTimeLink = System.nanoTime();
        for(int i = 0;i < count;i++)
            linkList.get(i);
        long estimatedTimeLink = System.nanoTime() - startTimeLink;

        System.out.println("ArrayList  = " + TimeUnit.NANOSECONDS.toMillis(estimatedTimeArr));
        System.out.println("LinkedList = " + TimeUnit.NANOSECONDS.toMillis(estimatedTimeLink));

    }

    long checkTimeFind(Collection c, int count){
        long startTime = System.nanoTime();

        Random random = new Random();

        for(int i = 0;i < count;i++){
            c.contains(random.nextInt());
        }

        return System.nanoTime() - startTime;
    }

    @Test
    void testSpeedCollectionsFind(){
        List<Integer> arrList = new ArrayList<>();
        Set<Integer> hashSet = new HashSet<>();
        Set<Integer> treeSet = new TreeSet<>();

        Random random = new Random();

        int count = 10000;

        while(hashSet.size() <= count){
            int r = random.nextInt();
            hashSet.add(r);
            treeSet.add(r);
            arrList.add(r);
        }

        System.out.println("ArrayList  = " + TimeUnit.NANOSECONDS.toMillis(checkTimeFind(arrList,count)));
        System.out.println("HashSet    = " + TimeUnit.NANOSECONDS.toMillis(checkTimeFind(hashSet,count)));
        System.out.println("TreeSet    = " + TimeUnit.NANOSECONDS.toMillis(checkTimeFind(treeSet,count)));

        arrList.clear();
        hashSet.clear();
        treeSet.clear();

        System.out.println("Тест без повторенний в ArrayList");

        while(hashSet.size() <= count){
            int r = random.nextInt();
            if(hashSet.add(r)){
                treeSet.add(r);
                arrList.add(r);
            }
        }

        System.out.println("ArrayList  = " + TimeUnit.NANOSECONDS.toMillis(checkTimeFind(arrList,count)));
        System.out.println("HashSet    = " + TimeUnit.NANOSECONDS.toMillis(checkTimeFind(hashSet,count)));
        System.out.println("TreeSet    = " + TimeUnit.NANOSECONDS.toMillis(checkTimeFind(treeSet,count)));

    }


    @Test
    void testBitSet(){
        BitSet bitSet = new BitSet();

        int size = 10;

        for(int i = 0;i < size;i++) {
            bitSet.set(i);
        }

        System.out.println(bitSet);

        Random random = new Random();

        int j = random.nextInt(size);

        assertTrue(bitSet.get(j));

        for(int i = 0;i <= 3;i++) {
            int r = random.nextInt(size);
            bitSet.clear(r);
        }

        System.out.println(bitSet);

    }

    @Test
    void testSpeedSetAdd(){
        BitSet bitSet = new BitSet();
        Set<Integer> hashSet = new HashSet<>();
        Set<Integer> treeSet = new TreeSet<>();

        int count = 1000000;

        long startTimeBitSet = System.nanoTime();
        for(int i = 0;i < count;i++)
            bitSet.set(i);
        long estimatedTimeBitSet = System.nanoTime() - startTimeBitSet;


        long startTimeHashSet = System.nanoTime();
        for(int i = 0;i < count;i++)
            hashSet.add(i);
        long estimatedTimeHashSet = System.nanoTime() - startTimeHashSet;

        long startTimeTreeSet = System.nanoTime();
        for(int i = 0;i < count;i++)
            treeSet.add(i);
        long estimatedTimeTreeSet = System.nanoTime() - startTimeTreeSet;

        System.out.println("BitSet     = " + TimeUnit.NANOSECONDS.toMillis(estimatedTimeBitSet));
        System.out.println("HashSet    = " + TimeUnit.NANOSECONDS.toMillis(estimatedTimeHashSet));
        System.out.println("TreeSet    = " + TimeUnit.NANOSECONDS.toMillis(estimatedTimeTreeSet));
    }


    @Test
    void testEnumSet(){
        EnumSet<Color> e1 = EnumSet.allOf(Color.class);
        EnumSet<Color> e2 = EnumSet.of(Color.GREEN);
        EnumSet<Color> e3 = EnumSet.range(Color.RED,Color.YELLOW);
        EnumSet<Color> e4 = EnumSet.noneOf(Color.class);

        System.out.println(e1);
        System.out.println(e2);
        System.out.println(e3);
        System.out.println(e4);
    }


}
