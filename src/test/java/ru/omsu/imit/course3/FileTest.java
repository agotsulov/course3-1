package ru.omsu.imit.course3;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class FileTest {

    @Test
    void testExistsFile(){
        assertTrue(new File("exists.txt").exists());
    }

    @Test
    void testCreateFile(){
        File file = new File("test.txt");
        try {
            file.createNewFile();
        } catch (IOException e) {
            fail();
        }
        assertTrue(new File("test.txt").exists());
    }

    @Test
    void testDeleteFile(){
        File file = new File("test.txt");
        try {
            file.createNewFile();
        } catch (IOException e) {
            fail();
        }
        file.delete();
        assertFalse(new File("test.txt").exists());
    }

    @Test
    void testRenameFile(){
        File file = new File("test.txt");
        try {
            file.createNewFile();
        } catch (IOException e) {
            fail();
        }
        file.renameTo(new File("rename"));
        assertTrue(new File("rename").exists());
    }


    @Test
    void testCreateDir(){
        File file = new File("testDir");
        file.mkdir();
        assertTrue(new File("testDir").exists());
    }

    @Test
    void testDeleteDir(){
        File file = new File("testDir");
        file.mkdir();
        file.delete();
        assertFalse(new File("testDir").exists());
    }

    @Test
    void testRenameDir(){
        File file = new File("testDir");
        file.mkdir();
        file.renameTo(new File("renameDir"));
        assertTrue(new File("renameDir").exists());
    }


    @Test
    void testFileGetFullName(){
        File file = new File("test.txt");
        String expect = "/home/byzilio/Workspace/Java/sim5/course3-1/test.txt";
        //assertEquals(expect, file.getAbsolutePath());
        System.out.println(file.getAbsolutePath());
    }

    @Test
    void testTypeFile(){
        File file = new File("testTypeFile");
        try {
            file.createNewFile();
        } catch (IOException e) {
            fail();
        }
        assertTrue(new File("testTypeFile").isFile());
    }

    @Test
    void testTypeDir(){
        File file = new File("testTypeDir");
        file.mkdir();
        assertTrue(new File("testTypeDir").isDirectory());
    }


    @Test
    void testGet(){
        File file = new File("testTypeDir");
        file.mkdir();
        assertTrue(new File("testTypeDir").isDirectory());
    }


    @Test
    void testGetAllFileInDir(){
        File folder = new File("testDirFiles");
        folder.mkdir();
        List<File> files = new ArrayList<>();
        for(int i = 0;i < 10;i++){
            File file = new File("testDirFiles/test" + Math.random() % 1000 + ".txt");
            try {
                file.createNewFile();
            } catch (IOException e) {
                fail();
            }
            files.add(file);
        }

        for(int i = 0;i < 13;i++){
            File file = new File("testDirFiles/test" + Math.random() * 100 + ".exe");
            try {
                file.createNewFile();
            } catch (IOException e) {
                fail();
            }
            files.add(file);
        }

        //assertEquals(Arrays.toString(files.toArray()),Arrays.toString(new File("testDirFiles").listFiles()) );
        System.out.println(Arrays.toString(new File("testDirFiles").listFiles()));
        System.out.println(Arrays.toString(new File("testDirFiles").listFiles(new FilenameFilter() {
            public boolean accept(File directory, String fileName) {
                return fileName.endsWith(".txt");
            }
        })));
    }



}


