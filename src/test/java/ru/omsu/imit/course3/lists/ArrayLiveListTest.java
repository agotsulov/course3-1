package ru.omsu.imit.course3.lists;

import org.junit.jupiter.api.Test;
import ru.omsu.imit.course3.animal.UniversalSpine;
import ru.omsu.imit.course3.core.LiveList;
import ru.omsu.imit.course3.entities.Animal;
import ru.omsu.imit.course3.entities.Plant;
import ru.omsu.imit.course3.plant.UniversalChlorophyll;

import static org.junit.jupiter.api.Assertions.*;

class ArrayLiveListTest {

    @Test
    void testAddAndGet() {
        LiveList actual = new ArrayLiveList();

        Animal expectAnimal = new Animal(new UniversalSpine(6));
        Plant expectPlant = new Plant(new UniversalChlorophyll(322));

        actual.add(new Animal(new UniversalSpine(1)));
        actual.add(new Animal(new UniversalSpine(2)));
        actual.add(new Animal(new UniversalSpine(3)));
        actual.add(new Animal(new UniversalSpine(4)));

        actual.add(expectAnimal);


        actual.add(new Plant(new UniversalChlorophyll(12512)));
        actual.add(new Plant(new UniversalChlorophyll(4536354)));

        actual.add(new Plant(new UniversalChlorophyll(12)));
        actual.add(new Plant(new UniversalChlorophyll(4)));

        actual.add(expectPlant);

        actual.add(new Animal(new UniversalSpine(1)));
        actual.add(new Animal(new UniversalSpine(124)));
        actual.add(new Animal(new UniversalSpine(125)));

        assertEquals(expectAnimal, actual.get(4));
        assertEquals(expectPlant, actual.get(9));
        assertNotEquals(expectAnimal, actual.get(6));
    }

    @Test
    void testAddByIndex() {
        LiveList actual = new ArrayLiveList();

        Animal expectAnimal = new Animal(new UniversalSpine(6));
        Plant expectPlant = new Plant(new UniversalChlorophyll(322));

        actual.add(new Animal(new UniversalSpine(1)));
        actual.add(new Animal(new UniversalSpine(2)));
        actual.add(new Animal(new UniversalSpine(3)));
        actual.add(new Animal(new UniversalSpine(4)));

        actual.add(1,expectAnimal);


        actual.add(new Plant(new UniversalChlorophyll(12512)));
        actual.add(new Plant(new UniversalChlorophyll(4536354)));

        actual.add(new Plant(new UniversalChlorophyll(12)));
        actual.add(new Plant(new UniversalChlorophyll(4)));

        actual.add(3,expectPlant);

        actual.add(new Animal(new UniversalSpine(1)));
        actual.add(new Animal(new UniversalSpine(124)));
        actual.add(new Animal(new UniversalSpine(125)));

        assertEquals(expectAnimal, actual.get(1));
        assertEquals(expectPlant, actual.get(3));
        assertNotEquals(expectAnimal, actual.get(6));
    }

    @Test
    void testRemove() {
        LiveList actual = new ArrayLiveList();

        Animal expect = new Animal(new UniversalSpine(6));

        actual.add(new Animal(new UniversalSpine(1)));
        actual.add(new Animal(new UniversalSpine(2)));
        actual.add(new Animal(new UniversalSpine(3)));
        actual.add(new Animal(new UniversalSpine(4)));

        actual.add(expect);

        actual.add(new Animal(new UniversalSpine(1)));
        actual.add(new Animal(new UniversalSpine(124)));
        actual.add(new Animal(new UniversalSpine(125)));

        actual.remove(3);
        actual.remove(1);

        assertEquals(expect, actual.get(2));
    }

    @Test
    void testContains() {
        LiveList actual = new ArrayLiveList();

        Animal expect = new Animal(new UniversalSpine(6));

        actual.add(new Animal(new UniversalSpine(1)));
        actual.add(new Animal(new UniversalSpine(2)));

        actual.add(expect);

        actual.add(new Animal(new UniversalSpine(1)));
        actual.add(new Animal(new UniversalSpine(124)));
        actual.add(new Animal(new UniversalSpine(125)));

        assertTrue(actual.contains(expect));
    }

    @Test
    void testRemoveByEntity() {
        LiveList actual = new ArrayLiveList();

        Animal expect = new Animal(new UniversalSpine(6));

        actual.add(new Animal(new UniversalSpine(1)));
        actual.add(new Animal(new UniversalSpine(2)));
        actual.add(new Animal(new UniversalSpine(3)));
        actual.add(new Animal(new UniversalSpine(4)));

        actual.add(expect);

        actual.add(new Animal(new UniversalSpine(1)));
        actual.add(new Animal(new UniversalSpine(124)));
        actual.add(new Animal(new UniversalSpine(125)));

        actual.remove(expect);

        assertFalse(actual.contains(expect));
    }


    @Test
    void testSize() {
        LiveList actual = new ArrayLiveList();

        actual.add(new Animal(new UniversalSpine(1)));
        actual.add(new Animal(new UniversalSpine(2)));
        actual.add(new Animal(new UniversalSpine(3)));
        actual.add(new Animal(new UniversalSpine(4)));
        actual.add(new Animal(new UniversalSpine(1)));
        actual.add(new Animal(new UniversalSpine(124)));
        actual.add(new Animal(new UniversalSpine(125)));

        assertEquals(7, actual.size());
    }

    @Test
    void testClean() {
        LiveList actual = new ArrayLiveList();

        actual.add(new Animal(new UniversalSpine(1)));
        actual.add(new Animal(new UniversalSpine(2)));
        actual.add(new Animal(new UniversalSpine(3)));
        actual.add(new Animal(new UniversalSpine(4)));
        actual.add(new Animal(new UniversalSpine(1)));
        actual.add(new Animal(new UniversalSpine(124)));
        actual.add(new Animal(new UniversalSpine(125)));

        actual.clean();

        assertEquals(0, actual.size());
    }


    @Test
    void testIsEmpty() {
        LiveList actual = new ArrayLiveList();

        actual.add(new Animal(new UniversalSpine(1)));
        actual.add(new Animal(new UniversalSpine(2)));
        actual.add(new Animal(new UniversalSpine(3)));
        actual.add(new Animal(new UniversalSpine(4)));
        actual.add(new Animal(new UniversalSpine(1)));
        actual.add(new Animal(new UniversalSpine(124)));
        actual.add(new Animal(new UniversalSpine(125)));


        assertFalse(actual.isEmpty());

        actual.clean();

        assertTrue(actual.isEmpty());
    }


}