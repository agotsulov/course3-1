package ru.omsu.imit.course3.entities;

import ru.omsu.imit.course3.animal.Spine;
import ru.omsu.imit.course3.core.Entity;
import ru.omsu.imit.course3.core.Type;

import java.util.Objects;

public class Animal extends Entity {

    private Spine spine;

    public Animal(Spine spine) {
        super(Type.ANIMAL);
        this.spine = spine;
    }

    public boolean haveSpine(){
        return spine != null;
    }

    public Spine getSpine() {
        return spine;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Animal animal = (Animal) o;
        return Objects.equals(spine, animal.spine);
    }

    @Override
    public int hashCode() {

        return Objects.hash(spine);
    }
}
