package ru.omsu.imit.course3.entities;

import ru.omsu.imit.course3.core.Entity;
import ru.omsu.imit.course3.core.Type;
import ru.omsu.imit.course3.plant.Chlorophyll;

import java.util.Objects;

public class Plant extends Entity {

    private Chlorophyll chlorophyll;

    public Plant(Chlorophyll chlorophyll) {
        super(Type.PLANT);
        this.chlorophyll = chlorophyll;
    }

    public boolean haveChlorophyll(){
        return  chlorophyll != null;
    }

    public Chlorophyll getChlorophyll() {
        return chlorophyll;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Plant plant = (Plant) o;
        return Objects.equals(chlorophyll, plant.chlorophyll);
    }

    @Override
    public int hashCode() {

        return Objects.hash(chlorophyll);
    }
}
