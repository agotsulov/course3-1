package ru.omsu.imit.course3.animal;

import java.util.Objects;

public class UniversalSpine implements Spine{

    private int length;

    public UniversalSpine(int length) {
        this.length = length;
    }

    public int getLength() {
        return length;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UniversalSpine that = (UniversalSpine) o;
        return length == that.length;
    }

    @Override
    public int hashCode() {

        return Objects.hash(length);
    }
}
