package ru.omsu.imit.course3.animal;

public interface Spine {

    int getLength();

}
