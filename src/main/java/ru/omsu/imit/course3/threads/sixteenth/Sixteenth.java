package ru.omsu.imit.course3.threads.sixteenth;

import org.apache.commons.cli.*;
import ru.omsu.imit.course3.threads.Executable;
import ru.omsu.imit.course3.threads.seventeen.QueueTask;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Sixteenth {

    private static class Developer extends Thread{

        private QueueExecutable qe;
        private Random random;

        public Developer(QueueExecutable qe) {
            this.qe = qe;
            random = new Random();
        }

        @Override
        public void run() {
            for(int i = 0;i < 50;i++) {
                qe.put(new PrintStringTask(Integer.toString(random.nextInt(100))));
            }
        }
    }

    private static class Executer extends Thread {
        private QueueExecutable qe;

        public Executer(QueueExecutable qe) {
            this.qe = qe;
        }

        @Override
        public void run() {
            while (true) {
                Executable e = qe.get();
                if(e instanceof Poison) {
                    break;
                }
                e.execute();
            }
        }
    }

    private static class Killer extends Thread {

        private QueueExecutable qe;
        private int c;

        public Killer(QueueExecutable qe, int c) {
            this.qe = qe;
            this.c = c;
        }

        @Override
        public void run() {
            for (int i = 0;i < c;i++) {
                qe.put(new Poison());
            }
        }
    }



    public static void main(String args[]) throws ParseException {
        Options options = new Options();

        Option devs = new Option("d", "dev", true, "Developers");
        Option exes = new Option("e", "exe", true, "Executers");

        options.addOption(devs);
        options.addOption(exes);

        CommandLineParser cmdLineParser = new DefaultParser();
        CommandLine cmd = cmdLineParser.parse(options, args);

        int countDevelopers = 5;
        int countExecuters = 5;

        if (cmd.hasOption("d")) {
            countDevelopers = Integer.parseInt(cmd.getOptionValue("d"));
        }

        if (cmd.hasOption("e")) {
            countExecuters = Integer.parseInt(cmd.getOptionValue("e"));
        }

        QueueExecutable qe = new QueueExecutable();

        List<Developer> developerList = new ArrayList<>();

        for (int i = 0;i < countDevelopers;i++) {
            Developer d = new Developer(qe);
            developerList.add(d);
            d.start();
        }

        List<Executer> executerList = new ArrayList<>();

        for (int i = 0;i < countExecuters;i++) {
            Executer executer = new Executer(qe);
            executerList.add(executer);
            executer.start();
        }

        for (int i = 0;i < countDevelopers;i++) {
            try {
                developerList.get(i).join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        Thread killer = new Killer(qe, countExecuters);
        killer.start();

        try {
            killer.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        for (int i = 0;i < countExecuters;i++) {
            try {
                executerList.get(i).join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
