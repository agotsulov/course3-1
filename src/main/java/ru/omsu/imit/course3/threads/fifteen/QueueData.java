package ru.omsu.imit.course3.threads.fifteen;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class QueueData {

    private BlockingQueue<Data> data;

    public QueueData() {
        data = new LinkedBlockingQueue<>();
    }

    public void put(Data d) {
        data.offer(d);
    }

    public Data get(){
        Data d = null;

        try {
            d = data.take();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        if (d instanceof Poison) {
            data.offer(d);
            return d;
        }

        return d;
    }

}
