package ru.omsu.imit.course3.threads.sixteenth;

import ru.omsu.imit.course3.threads.Executable;
import ru.omsu.imit.course3.threads.fifteen.Data;
import ru.omsu.imit.course3.threads.fifteen.Poison;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class QueueExecutable {

    private BlockingQueue<Executable> executables;

    public QueueExecutable() {
        executables = new LinkedBlockingQueue<>();
    }

    public void put(Executable e) {
        executables.offer(e);
    }

    public Executable get(){
        Executable e = null;

        try {
            e = executables.take();
        } catch (InterruptedException exc) {
            throw new RuntimeException(exc);
        }

        return e;
    }

}


