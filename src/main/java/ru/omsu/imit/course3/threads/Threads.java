package ru.omsu.imit.course3.threads;

import ru.omsu.imit.course3.threads.fourteenth.Formatter;
import ru.omsu.imit.course3.threads.fourteenth.Transport;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.*;

public class Threads {

    public void first() {
        Thread thread = Thread.currentThread();
        System.out.println(thread.toString());
    }

    /* ###################################################################################################### */

    public void second() {
        System.out.println("Main thread started");
        Thread thread = new Thread() {
            @Override
            public void run() {
                System.out.println("Other thread started");
                System.out.println("Other thread finished");
            }
        };
        thread.start();
        try{
            thread.join();
        }
        catch(InterruptedException e){
            e.printStackTrace();
        }
        System.out.println("Main thread finished");
    }

    /* ###################################################################################################### */

    public void third() {
        System.out.println("Main thread started");
        Thread thread1 = new Thread() {
            @Override
            public void run() {
                System.out.println("1 thread started");
                System.out.println("1 thread finished");
            }
        };
        Thread thread2 = new Thread() {
            @Override
            public void run() {
                System.out.println("2 thread started");
                System.out.println("2 thread finished");
            }
        };
        Thread thread3 = new Thread() {
            @Override
            public void run() {
                System.out.println("3 thread started");
                System.out.println("3 thread finished");
            }
        };

        thread1.start();
        thread2.start();
        thread3.start();
        try{
            thread1.join();
            thread2.join();
            thread3.join();
        }
        catch(InterruptedException e){
            e.printStackTrace();
        }
        System.out.println("Main thread finished");
    }

    /* ###################################################################################################### */

    private class ThreadAdd extends Thread{
        private List<Integer> integerList;

        public ThreadAdd(final List<Integer> integerList) {
            this.integerList = integerList;
        }

        @Override
        public void run() {
                Random random = new Random();
                for (int i = 0; i < 10000; i++) {
                    synchronized (integerList) {
                        integerList.add(random.nextInt());
                    }
                }
        }
    }

    private class ThreadRemove extends Thread{
        private List<Integer> integerList;

        public ThreadRemove(final List<Integer> integerList) {
            this.integerList = integerList;
        }

        @Override
        public void run() {
                Random random = new Random();
                for (int i = 0; i < 10000; i++) {
                    synchronized (integerList) {
                        integerList.remove(random.nextInt(integerList.size()));
                    }
                }
        }
    }

    public void four() {
        List<Integer> integerList = new ArrayList<>();

        Thread threadAdd = new ThreadAdd(integerList);
        Thread threadRemove = new ThreadRemove(integerList);

        threadAdd.start();
        threadRemove.start();
    }

    /* ###################################################################################################### */

    class IntegerList{

        List<Integer> integerList;

        public IntegerList() {
            integerList = new ArrayList<>();
        }

        public synchronized void add(int i) {
            integerList.add(i);
        }

        public synchronized void remove(int i) {
            if(integerList.size() > 0) {
                integerList.remove(i);
            }
        }

        public int size() {
            return integerList.size();
        }
    }

    private class AddThread extends Thread{
        private IntegerList integerList;

        public AddThread(final IntegerList integerList) {
            this.integerList = integerList;
        }

        @Override
        public void run() {
            Random random = new Random();
            for (int i = 0; i < 10000;i++) {
                integerList.add(random.nextInt());
            }
        }
    }

    private class RemoveThread extends Thread{
        private IntegerList integerList;

        public RemoveThread(final IntegerList integerList) {
            this.integerList = integerList;
        }

        @Override
        public void run() {
            Random random = new Random();
            for (int i = 0; i < 10000; i++) {
                integerList.remove(random.nextInt(integerList.size()));
            }
        }
    }

    public void fifth() {
        IntegerList integerList = new IntegerList();

        Thread threadAdd = new AddThread(integerList);
        Thread threadRemove = new RemoveThread(integerList);

        threadAdd.start();
        threadRemove.start();
    }

    /* ###################################################################################################### */

    public void sixth() {
        List<Integer> integerList =  Collections.synchronizedList(new ArrayList<>());

        synchronized (integerList) {
            Thread threadAdd = new ThreadAdd(integerList);
            Thread threadRemove = new ThreadRemove(integerList);

            threadAdd.start();
            threadRemove.start();
        }
    }

    /* ###################################################################################################### */

    private class PingFirst extends Thread {
        private Semaphore semCon;
        private Semaphore semProd;

        public PingFirst(Semaphore semCon, Semaphore semProd) {
            this.semCon = semCon;
            this.semProd = semProd;
        }

        public void run() {
            for (int i = 0;i < 30; i++) {
                try {
                    semCon.acquire();
                } catch (InterruptedException e) {
                    System.out.println("InterruptedException caught");
                }
                System.out.print("Ping ");
                semProd.release();
            }
        }
    }

    private class PongFirst extends Thread {
        private Semaphore semCon;
        private Semaphore semProd;

        public PongFirst(Semaphore semCon, Semaphore semProd) {
            this.semCon = semCon;
            this.semProd = semProd;
        }

        public void run() {
            for (int i = 0;i < 30; i++) {
                try {
                    semProd.acquire();
                } catch (InterruptedException e) {
                    System.out.println("InterruptedException caught");
                }
                System.out.println("pong");
                semCon.release();
            }
        }
    }

    public void seventh() {
        Semaphore semCon = new Semaphore(1);
        Semaphore semProd = new Semaphore(0);

        new PingFirst(semCon, semProd).start();
        new PongFirst(semCon, semProd).start();
    }

    /* ###################################################################################################### */


    private class IO {
        private String s;
        private Semaphore semCon = new Semaphore(0);
        private Semaphore semProd = new Semaphore(1);

        public String read() {
            try {
                semCon.acquire();
            } catch (InterruptedException e) {
                System.out.println("InterruptedException caught");
            }
            System.out.println("IO Read: " + s);
            semProd.release();
            return s;
        }

        public void write(String s) {
            try {
                semProd.acquire();
            } catch (InterruptedException e) {
                System.out.println("InterruptedException caught");
            }
            System.out.println("IO Write: " + s);
            this.s = s;
            semCon.release();
        }
    }

    private class Writer extends Thread {
        private IO io;
        private Random random;

        public Writer(IO io) {
            this.io = io;
            random = new Random();
        }

        public void run() {
            for(int i = 0; i < 20;i++) {
                String s = Integer.toString(random.nextInt());
                //System.out.println("Write: " + s);
                io.write(s);
            }
        }
    }

    private class Reader extends Thread {
        private IO io;

        public Reader(IO io) {
            this.io = io;
        }

        public void run() {
            for(int i = 0; i < 20;i++) {
                //System.out.println("Read: " + io.read());
                io.read();
            }
        }
    }

    public void eighth() {
        IO io = new IO();

        Thread writer = new Writer(io);
        Thread reader = new Reader(io);

        writer.start();
        reader.start();
    }


    /* ###################################################################################################### */

    private class ThreadAddReentrantLock extends Thread{
        private List<Integer> integerList;
        private ReentrantLock lock;

        public ThreadAddReentrantLock(final List<Integer> integerList, final ReentrantLock lock) {
            this.integerList = integerList;
            this.lock = lock;
        }

        @Override
        public void run() {
            lock.lock();
            try {
                Random random = new Random();
                for (int i = 0; i < 10000; i++) {
                    integerList.add(random.nextInt());
                }
            } finally {
                lock.unlock();
            }
        }
    }

    private class ThreadRemoveReentrantLock extends Thread{
        private List<Integer> integerList;
        private ReentrantLock lock;

        public ThreadRemoveReentrantLock(final List<Integer> integerList, final ReentrantLock lock) {
            this.integerList = integerList;
            this.lock = lock;
        }

        @Override
        public void run() {
            lock.lock();
            try {
                Random random = new Random();
                for (int i = 0; i < 10000; i++) {
                    if (integerList.size() > 0) {
                        integerList.remove(random.nextInt(integerList.size()));
                    }
                }
            } finally {
                lock.unlock();
            }
        }
    }

    public void tenth() {
        List<Integer> integerList = new ArrayList<>();
        ReentrantLock lock = new ReentrantLock();
        Thread threadAdd = new ThreadAddReentrantLock(integerList, lock);
        Thread threadRemove = new ThreadRemoveReentrantLock(integerList, lock);

        threadAdd.start();
        threadRemove.start();
    }

    /* ###################################################################################################### */


    private class PingPong {
        private Lock lock = new ReentrantLock();
        private Condition condition1 = lock.newCondition();
        private Condition condition2 = lock.newCondition();

        private boolean send = true;

        public void ping() throws InterruptedException {
            lock.lock();
            try {
                while (!send) {
                    condition1.await();
                }
                System.out.print("ping ");
                send = false;
                condition2.signal();
            } finally {
                lock.unlock();
            }
        }

        public void pong() throws InterruptedException {
            lock.lock();
            try {
                while (send) {
                    condition2.await();
                }
                System.out.println(" pong");
                send = true;
                condition1.signal();
            } finally {
                lock.unlock();
            }
        }
    }

    private class PingSecond extends Thread {
        private PingPong pingPong;

        public PingSecond(PingPong pingPong) {
            this.pingPong = pingPong;
        }

        @Override
        public void run() {
            try {
                for (int i = 0; i < 30; i++) {
                    pingPong.ping();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private class PongSecond extends Thread {
        private PingPong pingPong;

        public PongSecond(PingPong pingPong) {
            this.pingPong = pingPong;
        }

        @Override
        public void run() {
            try {
                for (int i = 0; i < 30; i++) {
                    pingPong.pong();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void eleventh() {
        System.out.println("Main thread started");

        PingPong pingPong = new PingPong();

        Thread ping = new PingSecond(pingPong);
        Thread pong = new PongSecond(pingPong);

        ping.start();
        pong.start();

        try {
            ping.join();
            pong.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Main thread finished");
    }

    /* ###################################################################################################### */

    private class MyConcurrentHashMap<K, V> implements Map<K, V> {
        private HashMap<K, V> hashMap;
        private ReadWriteLock lock = new ReentrantReadWriteLock();

        public MyConcurrentHashMap() {
            hashMap = new HashMap<>();
        }

        @Override
        public int size() {
            lock.readLock().lock();
            try{
                return hashMap.size();
            } finally {
                lock.readLock().unlock();
            }
        }

        @Override
        public boolean isEmpty() {
            lock.readLock().lock();
            try {
                return hashMap.isEmpty();
            } finally {
                lock.readLock().unlock();
            }
        }

        @Override
        public boolean containsKey(Object key) {
            lock.readLock().lock();
            try {
                return hashMap.containsValue(key);
            } finally {
                lock.readLock().unlock();
            }
        }

        @Override
        public boolean containsValue(Object value) {
            lock.readLock().lock();
            try {
                return hashMap.containsValue(value);

            } finally {
                lock.readLock().unlock();

            }
        }

        @Override
        public V get(Object key) {
            lock.readLock().lock();
            try {
                return hashMap.get(key);
            } finally {
                lock.readLock().unlock();
            }
        }

        @Override
        public V put(K key, V value) {
            lock.writeLock().lock();
            try {
                return hashMap.put(key, value);
            } finally {
                lock.writeLock().unlock();
            }
        }

        @Override
        public V remove(Object key) {
            lock.readLock().lock();
            try {
                return hashMap.remove(key);
            } finally {
                lock.readLock().unlock();
            }
        }

        @Override
        public void putAll(Map<? extends K, ? extends V> m) {
            lock.writeLock().lock();
            try {
                hashMap.putAll(m);
            } finally {
                lock.writeLock().unlock();
            }
        }

        @Override
        public void clear() {
            lock.writeLock().lock();
            try {
                hashMap.clear();
            } finally {
                lock.writeLock().unlock();
            }
        }

        @Override
        public Set<K> keySet() {
            lock.readLock().lock();
            try {
                return hashMap.keySet();
            } finally {
                lock.readLock().unlock();
            }
        }

        @Override
        public Collection<V> values() {
            lock.readLock().lock();
            try {
                return hashMap.values();
            } finally {
                lock.readLock().unlock();
            }
        }

        @Override
        public Set<Entry<K, V>> entrySet() {
            lock.readLock().lock();

            try {
                return hashMap.entrySet();
            } finally {
                lock.readLock().unlock();
            }
        }
    }

    /* ###################################################################################################### */

    private class FormatterThread extends Thread{
        private ru.omsu.imit.course3.threads.fourteenth.Formatter formatter;

        public FormatterThread(ru.omsu.imit.course3.threads.fourteenth.Formatter formatter) {
            this.formatter = formatter;
        }

        @Override
        public void run() {
            while (true) {
                System.out.println(formatter.format(new Date()));
            }
        }
    }


    public void thirteenth() {
        ru.omsu.imit.course3.threads.fourteenth.Formatter formatter = new Formatter();

        new FormatterThread(formatter).start();
        new FormatterThread(formatter).start();
        new FormatterThread(formatter).start();
        new FormatterThread(formatter).start();
        new FormatterThread(formatter).start();
    }


    /* ###################################################################################################### */

    private class MessageSender extends Thread {
        private Transport transport;

        public MessageSender(Transport transport) {
            this.transport = transport;
        }

        @Override
        public void run() {
            while(transport.hasMessage()) {
                try {
                    transport.send(transport.next());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void fourteenth() throws IOException {
        Transport transport = new Transport("in.txt", "out.txt", "AAAAAAAAAAAAAAA", "bbbb"
                , "aaa@aaa.ccc");

        new MessageSender(transport).start();
        new MessageSender(transport).start();
        new MessageSender(transport).start();
        new MessageSender(transport).start();
        new MessageSender(transport).start();
        new MessageSender(transport).start();

        transport.close();
    }

    /* ###################################################################################################### */



}
