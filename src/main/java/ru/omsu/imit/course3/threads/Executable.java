package ru.omsu.imit.course3.threads;

public interface Executable {

    void execute();
}
