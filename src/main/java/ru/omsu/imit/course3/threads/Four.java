package ru.omsu.imit.course3.threads;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Four {

    private class ThreadAdd extends Thread{
        private List<Integer> integerList;

        public ThreadAdd(final List<Integer> integerList) {
            this.integerList = integerList;
        }

        @Override
        public void run() {
            Random random = new Random();
            for (int i = 0; i < 10000; i++) {
                synchronized (integerList) {
                    integerList.add(random.nextInt());
                }
            }
        }
    }

    private class ThreadRemove extends Thread{
        private List<Integer> integerList;

        public ThreadRemove(final List<Integer> integerList) {
            this.integerList = integerList;
        }

        @Override
        public void run() {
            Random random = new Random();
            for (int i = 0; i < 10000; i++) {
                synchronized (integerList) {
                    integerList.remove(random.nextInt(integerList.size()));
                }
            }
        }
    }

    public void four() {
        List<Integer> integerList = new ArrayList<>();

        Thread threadAdd = new ThreadAdd(integerList);
        Thread threadRemove = new ThreadRemove(integerList);

        threadAdd.start();
        threadRemove.start();
    }
}
