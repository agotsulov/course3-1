package ru.omsu.imit.course3.threads.sixteenth;

import ru.omsu.imit.course3.threads.Executable;

public class PrintStringTask implements Executable {

    private String s;

    public PrintStringTask(String s) {
        this.s = s;
        System.out.println("PUT: " + s);
    }

    @Override
    public void execute() {
        System.out.println("GET: " + s);
    }
}
