package ru.omsu.imit.course3.threads.fifteen;

import org.apache.commons.cli.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Fifteen {

    private static class Writer extends Thread{
        private QueueData qd;
        private Random random;

        public Writer(QueueData qd) {
            this.qd = qd;
            random = new Random();
        }

        @Override
        public void run() {
            for(int i = 0;i < 10;i++) {
                int size = random.nextInt(3) + 1;
                int[] d = new int[size];
                for (int j = 0;j < d.length;j++) {
                    d[j] = random.nextInt(10);
                }
                System.out.println(getName() + " PUT " + Arrays.toString(d));
                qd.put(new Data(d));
            }
        }
    }

    private static class Reader extends Thread {
        private QueueData qd;

        public Reader(QueueData qd) {
            this.qd = qd;
        }

        @Override
        public void run() {
            while (true) {
                Data d = qd.get();
                if (d instanceof Poison) {
                    break;
                }
                System.out.println(this.getName() + " GET " + Arrays.toString(d.get()));
            }
        }
    }

    private static class Killer extends Thread {

        private QueueData qd;

        public Killer(QueueData qd) {
            this.qd = qd;
        }

        @Override
        public void run() {
            qd.put(new Poison());
        }
    }


    public static void main(String args[]) throws ParseException {
        Options options = new Options();

        Option writers = new Option("w", "write", true, "Writers");
        Option readers = new Option("r", "read", true, "Readers");

        options.addOption(writers);
        options.addOption(readers);

        CommandLineParser cmdLineParser = new DefaultParser();
        CommandLine cmd = cmdLineParser.parse(options, args);

        int countWriters = 5;
        int countReaders = 5;

        if (cmd.hasOption("w")) {
            countWriters = Integer.parseInt(cmd.getOptionValue("w"));
        }

        if (cmd.hasOption("r")) {
            countReaders = Integer.parseInt(cmd.getOptionValue("e"));
        }

        QueueData qd = new QueueData();

        List<Writer> writerList = new ArrayList<>();

        for (int i = 0;i < countWriters;i++) {
            Writer w = new Writer(qd);
            writerList.add(w);
            w.start();
        }

        List<Reader> readersList = new ArrayList<>();

        for (int i = 0;i < countReaders;i++) {
            Reader reader = new Reader(qd);
            readersList.add(reader);
            reader.start();
        }

        for (int i = 0;i < countWriters;i++) {
            try {
                writerList.get(i).join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        Killer killer = new Killer(qd);
        killer.start();

        try {
            killer.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        for (int i = 0;i < countReaders;i++) {
            try {
                readersList.get(i).join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


    }


}
