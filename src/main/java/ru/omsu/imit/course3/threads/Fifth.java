package ru.omsu.imit.course3.threads;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Fifth {
    private class IntegerList{

        List<Integer> integerList;

        public IntegerList() {
            integerList = new ArrayList<>();
        }

        public synchronized void add(int i) {
            integerList.add(i);
        }

        public synchronized void remove(int i) {
            integerList.remove(i);
        }

        public int size() {
            return integerList.size();
        }
    }

    private class AddThread extends Thread{
        private IntegerList integerList;

        public AddThread(final IntegerList integerList) {
            this.integerList = integerList;
        }

        @Override
        public void run() {
            Random random = new Random();
            for (int i = 0; i < 10000;i++) {
                integerList.add(random.nextInt());
            }
        }
    }

    private class RemoveThread extends Thread{
        private IntegerList integerList;

        public RemoveThread(final IntegerList integerList) {
            this.integerList = integerList;
        }

        @Override
        public void run() {
            if(integerList.size() > 0) {
                Random random = new Random();
                for (int i = 0; i < 10000; i++) {
                    integerList.remove(random.nextInt(integerList.size()));
                }
            }
        }
    }

    public void fifth() {
        IntegerList integerList = new IntegerList();

        Thread threadAdd = new AddThread(integerList);
        Thread threadRemove = new RemoveThread(integerList);

        threadAdd.start();
        threadRemove.start();
    }

}
