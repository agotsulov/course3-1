package ru.omsu.imit.course3.threads.seventeen;

import org.apache.commons.cli.*;
import ru.omsu.imit.course3.threads.Executable;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Seventeen {

    private static class Developer extends Thread {

        private QueueTask qt;
        private Random random;
        private int count;

        public Developer(QueueTask qt, String name, int count) {
            this.qt = qt;
            random = new Random();
            this.count = count;
            setName(name);
        }

        @Override
        public void run() {
            for (int i = 0;i < count;i++) {
                List<Executable> exe = new ArrayList<>();
                int size = random.nextInt(5) + 1;
                int t = 0;
                for (int j = 0;j < size;j++) {
                    exe.add(new PrintStringExe(this.getName() + " " + t + " stage"));
                    t++;
                }
                qt.addTask(new Task(exe));
            }
        }
    }

    private static class Executor extends Thread {

        private QueueTask tq;

        public Executor(QueueTask qt) {
            this.tq = qt;
        }

        @Override
        public void run() {
            boolean check = true;
            while (check) {
                check = tq.execute();
            }
        }
    }

    private static class Observer extends Thread {

        private QueueTask tq;

        public Observer(QueueTask tq) {
            this.tq = tq;
        }

        @Override
        public void run() {
            while(true) {
                try {
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                    return;
                }
                Task current = tq.peek();
                if (current == null)
                    System.out.println("NO TASKS");
                else {
                    System.out.println("NAME: " + current + " STAGE: " + current.getCurrentStage() + " COUNT: " + current.getStages().size());
                }
            }
        }
    }

    private static class Killer extends Thread {

        private QueueTask tq;

        public Killer(QueueTask tq) {
            this.tq = tq;
        }

        @Override
        public void run() {
            try {
                tq.getSignal().take();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            tq.addTask(new Poison());
        }
    }

    public static void main(String[] args) throws ParseException {
        Options options = new Options();

        Option devs = new Option("d", "dev", true, "Developers");
        Option exes = new Option("e", "exe", true, "Executers");

        options.addOption(devs);
        options.addOption(exes);

        CommandLineParser cmdLineParser = new DefaultParser();
        CommandLine cmd = cmdLineParser.parse(options, args);

        int countDevelopers = 2;
        int countExecuters = 5;

        if (cmd.hasOption("d")) {
            countDevelopers = Integer.parseInt(cmd.getOptionValue("d"));
        }

        if (cmd.hasOption("e")) {
            countExecuters = Integer.parseInt(cmd.getOptionValue("e"));
        }


        QueueTask tq = new QueueTask();

        List<Thread> developers = new ArrayList<>();

        for (int i = 0; i < countExecuters; i++)
            new Thread(new Executor(tq)).start();

        for (int i = 0; i < countDevelopers; i++) {
            developers.add(new Thread(new Developer(tq, "Developer " + i, 10)));
            developers.get(i).start();
        }

        Thread observer = new Observer(tq);
        observer.start();

        for (int i = 0; i < countDevelopers; i++) {
            try {
                developers.get(i).join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        Thread killer = new Killer(tq);
        killer.start();

        try {
            killer.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        observer.interrupt();
    }

}
