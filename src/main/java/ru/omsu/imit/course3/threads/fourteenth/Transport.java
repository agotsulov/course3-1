package ru.omsu.imit.course3.threads.fourteenth;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Transport {

    private FileWriter fileWriter;

    private List<Message> messageList;
    private int i;

    public Transport(String in, String out, String messageText, String subject, String sender) throws IOException {
        fileWriter = new FileWriter(out);

        messageList = new ArrayList<>();

        BufferedReader b = new BufferedReader(new FileReader(new File(in)));

        String readLine = "";

        while ((readLine = b.readLine()) != null) {
            messageList.add(new Message(readLine, sender, subject, messageText));
        }

        b.close();

    }

    public void send(Message message) throws IOException {
        if (message != null) {
            fileWriter.write(message.toString() + "\n");
            fileWriter.flush();
        }
    }

    public void close() throws IOException {
        fileWriter.close();
    }

    public boolean hasMessage() {
        return i < messageList.size();
    }

    public Message next() {
        if (hasMessage()) {
            Message m = messageList.get(i);
            i++;
            return m;
        }
        return null;
    }


}
