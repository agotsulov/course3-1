package ru.omsu.imit.course3.threads.seventeen;

import ru.omsu.imit.course3.threads.Executable;

public class PrintStringExe implements Executable {

    private String s;

    public PrintStringExe(String s) {
        this.s = s;
    }

    @Override
    public void execute() {
        System.out.println(s);
    }
}
