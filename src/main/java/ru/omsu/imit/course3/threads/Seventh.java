package ru.omsu.imit.course3.threads;

import java.util.concurrent.Semaphore;

public class Seventh {

    private class PingFirst extends Thread {
        private Semaphore semCon;
        private Semaphore semProd;

        public PingFirst(Semaphore semCon, Semaphore semProd) {
            this.semCon = semCon;
            this.semProd = semProd;
        }

        public void run() {
            for (int i = 0;i < 30; i++) {
                try {
                    semCon.acquire();
                } catch (InterruptedException e) {
                    System.out.println("InterruptedException caught");
                }
                System.out.print("Ping ");
                semProd.release();
            }
        }
    }

    private class PongFirst extends Thread {
        private Semaphore semCon;
        private Semaphore semProd;

        public PongFirst(Semaphore semCon, Semaphore semProd) {
            this.semCon = semCon;
            this.semProd = semProd;
        }

        public void run() {
            for (int i = 0;i < 30; i++) {
                try {
                    semProd.acquire();
                } catch (InterruptedException e) {
                    System.out.println("InterruptedException caught");
                }
                System.out.println("pong");
                semCon.release();
            }
        }
    }

    public void seventh() {
        Semaphore semCon = new Semaphore(1);
        Semaphore semProd = new Semaphore(0);

        new PingFirst(semCon, semProd).start();
        new PongFirst(semCon, semProd).start();
    }
}
