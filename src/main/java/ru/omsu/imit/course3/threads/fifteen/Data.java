package ru.omsu.imit.course3.threads.fifteen;

public class Data {

    private int[] data;

    public Data(int[] data) {
        this.data = data;
    }

    public int[] get(){
        return data;
    }

}
