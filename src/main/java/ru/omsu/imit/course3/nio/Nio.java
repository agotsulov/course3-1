package ru.omsu.imit.course3.nio;

import ru.omsu.imit.course3.trianee.Trainee;
import ru.omsu.imit.course3.trianee.TrainingException;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.EnumSet;

public class Nio {

    public static Trainee writeTraineeToFile(String filename) throws TrainingException, IOException {
        String string = null;

        try(FileChannel channel = new FileInputStream(filename).getChannel()) {
            ByteBuffer buf = ByteBuffer.allocate((int) channel.size());
            channel.read(buf);

            string = new String(buf.array(), Charset.forName("UTF-8"));

            System.out.println(string);
        }

        String[] strings = string.split(" ");

        return new Trainee(strings[0], strings[1], Integer.parseInt(strings[2]));

    }

    public static Trainee readTraineeToFileMappedByteBuffer(String filename) throws IOException, TrainingException {
        StringBuilder string = new StringBuilder();

        CharBuffer charBuffer = null;

        try (FileChannel fileChannel = new FileInputStream(filename).getChannel()) {
            MappedByteBuffer mappedByteBuffer = fileChannel
                    .map(FileChannel.MapMode.READ_ONLY, 0, fileChannel.size());

            if (mappedByteBuffer != null) {
                charBuffer = Charset.forName("UTF-8").decode(mappedByteBuffer);
                System.out.println(charBuffer);
                string.append(charBuffer);
            }
        }
        String[] strings = string.toString().split(" ");

        return new Trainee(strings[0], strings[1], Integer.parseInt(strings[2]));

    }


    public static void writeHundredInFile(Path filePath) throws IOException {
        try (FileChannel fileChannel = (FileChannel) Files.newByteChannel(filePath, EnumSet.of(
                StandardOpenOption.READ,
                StandardOpenOption.WRITE))) {

            MappedByteBuffer mappedByteBuffer = fileChannel
                    .map(FileChannel.MapMode.READ_WRITE, 0, 8 * 99);

            if (mappedByteBuffer != null) {
                for (int i = 0; i < 100; i++) {
                    mappedByteBuffer.putInt(i);
                }
            }
        }
    }

    public static void serializeTrainee(Trainee trainee) throws IOException {
        try (ByteArrayOutputStream bytesOut = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(bytesOut)) {
            oos.writeObject(trainee);
            oos.flush();
            ByteBuffer bytes = ByteBuffer.wrap(bytesOut.toByteArray());
            System.out.println(new String(bytes.array()));
        }
    }

    public static void renameDatToBin(Path rootPath) throws IOException {
        Files.walkFileTree(rootPath, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                String fileString = file.toString();
                System.out.println("pathString = " + fileString);

                if (fileString.endsWith(".dat")) {
                    Path target = Paths.get(fileString.substring(0, fileString.length() - 3) + "bin");

                    Files.move(file, target, StandardCopyOption.REPLACE_EXISTING);
                }
                return FileVisitResult.CONTINUE;
            }
        });

    }

}


