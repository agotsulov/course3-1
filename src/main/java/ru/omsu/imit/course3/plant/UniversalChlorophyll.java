package ru.omsu.imit.course3.plant;

import java.util.Objects;

public class UniversalChlorophyll implements Chlorophyll{

    private int count;

    public UniversalChlorophyll(int count) {
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UniversalChlorophyll that = (UniversalChlorophyll) o;
        return count == that.count;
    }

    @Override
    public int hashCode() {

        return Objects.hash(count);
    }
}
