package ru.omsu.imit.course3.lists;

import ru.omsu.imit.course3.core.Entity;
import ru.omsu.imit.course3.core.LiveList;

import java.util.ArrayList;
import java.util.List;

public class ArrayLiveList implements LiveList{

    private List<Entity> entities = new ArrayList<Entity>();

    public boolean add(Entity e) {
        return entities.add(e);
    }

    public void add(int index, Entity e) {
        entities.add(index, e);
    }

    public Entity get(int i) {
        return entities.get(i);
    }

    public void clean() {
        entities.clear();
    }

    public boolean isEmpty() {
        return entities.isEmpty();
    }

    public Entity remove(int index) {
        return entities.remove(index);
    }

    public boolean remove(Entity e) {
        return entities.remove(e);
    }

    @Override
    public int size() {
        return entities.size();
    }

    @Override
    public boolean contains(Entity e) {
        return entities.contains(e);
    }
}
