package ru.omsu.imit.course3.trianee;

import java.util.*;

public class Group {

    private List<Trainee> trainees;

    public Group(List<Trainee> list) throws TrainingException {
        trainees = list;
    }

    public List<Trainee> getTrainees() {
        return trainees;
    }

    public void addTrainee(Trainee trainee){
        trainees.add(trainee);
    }

    public void  removeTrainee(Trainee trainee) throws TrainingException {
        if(!trainees.remove(trainee)) throw new TrainingException(TrainingErrorCode.TRAINEE_NOT_FOUND);
    }

    public void  removeTrainee(int index) throws TrainingException {
        try{
            trainees.remove(index);
        } catch (IndexOutOfBoundsException i){
            throw new TrainingException(TrainingErrorCode.TRAINEE_NOT_FOUND);
        }
    }

    public Trainee  getTraineeByFirstName(String firstName) throws TrainingException {
        for(Trainee t:trainees)
            if(t.getFirstName().equals(firstName)) return t;
        throw new TrainingException(TrainingErrorCode.TRAINEE_NOT_FOUND);
    }

    public Trainee  getTraineeByFullName(String fullName) throws TrainingException {
        for(Trainee t:trainees)
            if(t.getFullName().equals(fullName)) return t;
        throw new TrainingException(TrainingErrorCode.TRAINEE_NOT_FOUND);
    }

    public void  sortTraineeListByFirstNameAscendant(){
        trainees.sort(Comparator.comparing(Trainee::getFirstName));
    }
    //Сортирует список Trainee группы, упорядочивая его по возрастанию имени Trainee.


    public void  sortTraineeListByRatingDescendant(){
        trainees.sort(Comparator.comparing(Trainee::getRating).reversed());
    }
    //Сортирует список Trainee группы, упорядочивая его по убыванию оценки Trainee.


    public void  reverseTraineeList(){
        Collections.reverse(trainees);
    }
    //Переворачивает список Trainee группы, то есть последний элемент списка становится начальным, предпоследний - следующим за начальным и т.д..


    public void  rotateTraineeList(int positions){
        Collections.rotate(trainees,positions);
    }
    //Циклически сдвигает список Trainee группы на указанное число позиций. Для положительного значения positions сдвигает вправо, для отрицательного - влево на модуль значения positions.

    public List<Trainee> getTraineesWithMaxRating() throws TrainingException {
        if(trainees.isEmpty()) throw new TrainingException(TrainingErrorCode.TRAINEE_NOT_FOUND);
        List<Trainee> result = new ArrayList<>();
        int maxRating = 0;
        for(Trainee t:trainees){
            if(t.getRating() > maxRating){
                result.clear(); //Тут чистим, если нашли больший рейтинг. У меня тест с 1,2,3,4 работет
                maxRating = t.getRating();
                result.add(t);
            } else if(t.getRating() == maxRating)
                result.add(t);
        }
        return result;
    }
    //Возвращает список тех Trainee группы , которые имеют наивысшую оценку. Иными словами, если в группе есть Trainee с оценкой 5, возвращает список получивших оценку 5, если же таких нет, но есть Trainee с оценкой 4, возвращает список получивших оценку 4 и т.д. Для пустого списка выбрасывает TrainingException с TrainingErrorCode.TRAINEE_NOT_FOUND

    public boolean  hasDuplicates(){
        for(Trainee t:trainees)
            if(Collections.frequency(trainees, t) > 1) return true;
        return false;
    }
    //Проверяет, есть ли в группе хотя бы одна пара Trainee, для которых совпадают имя, фамилия и оценка.


    public void shift(int count){
        List<Trainee> list = new ArrayList<>();
        Collections.copy(list, trainees);
        trainees.clear();
        for(int i = 0;i < list.size();i++){
            trainees.add(i + count,list.get(i));
        }
    }

    public void shuffle(){
        Collections.shuffle(trainees);
    }
}

