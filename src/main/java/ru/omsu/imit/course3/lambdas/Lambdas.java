package ru.omsu.imit.course3.lambdas;

import ru.omsu.imit.course3.person.Person;
import ru.omsu.imit.course3.person.PersonWithAge;

import java.util.*;
import java.util.function.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Lambdas{

    private MyFunction<String, List<String>> split = (s) -> Arrays.asList(s.split(" "));

    private MyFunction<List<?>, Integer> count = (list) -> list.size();

    private MyFunction<List<?>, Integer> countMR = List::size;

    public List<String> split(String string) {
        return split.apply(string);
    }

    private MyFunction<String, Integer> splitAndCountA = (s) -> split.andThen(count).apply(s);

    private MyFunction<String, Integer> splitAndCountB = (s) -> count.compose(split).apply(s);

    private MyFunction<String, Person> create = Person::new;

    private IntBinaryOperator max = Math::max;

    private Supplier<Date> getCurrentDate = Date::new;

    private IntPredicate isEven = (n) -> n % 2 == 0;

    private BiPredicate<Integer, Integer> areEqual = Integer::equals;

    @FunctionalInterface
    interface MyFunction<T,K> extends Function<T,K> {
        K apply(T arg);
    }

    public void transform(IntStream stream, IntUnaryOperator op) {
        stream.map(op).forEach(System.out::println);
    }

    public void transformParallel(IntStream stream, IntUnaryOperator op) {
        stream.parallel().map(op).forEach(System.out::println);
    }

    public List<PersonWithAge> printPersonsOlderThirty (List<PersonWithAge> persons) {
        return persons.stream()
                .filter((p) -> p.getAge() > 30)
                .collect(Collectors.toSet())
                .stream()
                .sorted(Comparator.comparingInt(a -> a.getName().length()))
                .collect(Collectors.toList());
    }

    public List<String> printPersonsOlderThirtyWithEquals (List<PersonWithAge> persons ) {
       return persons.stream().filter((p) -> p.getAge() > 30)
               .collect(Collectors.groupingBy(Person::getName))
               .entrySet()
               .stream()
               .sorted(Comparator.comparingInt(a -> a.getKey().length()))
               .map(Map.Entry::getKey)
               .collect(Collectors.toList());
    }

    public int sum(List<Integer> list) {
        return list.stream().reduce((s1, s2) -> s1 + s2).get();
    }

    public int product(List<Integer> list) {
        return list.stream().reduce((s1, s2) -> s1 * s2).get();
    }

}



