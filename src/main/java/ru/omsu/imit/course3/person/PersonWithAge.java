package ru.omsu.imit.course3.person;

public class PersonWithAge extends Person{

    private int age;

    public PersonWithAge(String name, int age) {
        super(name);
        this.age = age;
    }

    public int getAge() {
        return age;
    }

}
