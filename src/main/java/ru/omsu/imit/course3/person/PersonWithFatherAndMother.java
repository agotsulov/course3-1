package ru.omsu.imit.course3.person;

public class PersonWithFatherAndMother extends Person{

    private PersonWithFatherAndMother father;
    private PersonWithFatherAndMother mother;

    public PersonWithFatherAndMother(String name, PersonWithFatherAndMother father, PersonWithFatherAndMother mother) {
        super(name);
        this.father = father;
        this.mother = mother;
    }

    public PersonWithFatherAndMother getFather() {
        return father;
    }

    public PersonWithFatherAndMother getMother() {
        return mother;
    }

    public Person getMothersMotherFather() {
        Person result = null;

        result = mother.getMother().getMother().getFather();

        if(result == null) {
            throw new NullPointerException();
        }

        return result;
    }
}
