package ru.omsu.imit.course3.person;

import java.util.Optional;

public class PersonOptionalFatherAndMother extends Person{

    private Optional<PersonOptionalFatherAndMother> father;
    private Optional<PersonOptionalFatherAndMother> mother;

    public PersonOptionalFatherAndMother(String name, PersonOptionalFatherAndMother father, PersonOptionalFatherAndMother mother) {
        super(name);
        this.father = Optional.ofNullable(father);
        this.mother = Optional.ofNullable(mother);
    }

    public Optional<PersonOptionalFatherAndMother> getFather() {
        return father;
    }

    public Optional<PersonOptionalFatherAndMother> getMother() {
        return mother;
    }

    public Optional<PersonOptionalFatherAndMother> getMothersMotherFather() {
        Optional<PersonOptionalFatherAndMother> result = null;

        result = mother.flatMap((r) -> r.getMother().flatMap((t) -> t.getMother().flatMap((g) -> g.getFather())));

        return result;
    }
}
