package ru.omsu.imit.course3.core;

import java.util.List;

public interface LiveList {

    boolean add(Entity e);

    void add(int index, Entity e);

    Entity get(int i);

    void clean();

    boolean isEmpty();

    Entity remove(int index);

    boolean remove(Entity e);

    int size();

    boolean contains(Entity e);

}
