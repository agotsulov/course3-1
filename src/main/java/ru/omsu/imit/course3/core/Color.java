package ru.omsu.imit.course3.core;

public enum Color {

    RED,
    GREEN,
    BLUE,
    YELLOW,
    GRAY
}
