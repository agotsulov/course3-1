package ru.omsu.imit.course3.core;


public abstract class Entity {

    private Type type;

    public Entity(Type type) {
        this.type = type;
    }

    public Type getType() {
        return type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Entity entity = (Entity) o;
        return type == entity.type;
    }

}